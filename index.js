const express = require('express')
const app = express()
const mongoose = require('mongoose')
const { ApolloServer} = require('apollo-server-express')

mongoose.connection.once('open', ()=>{

	console.log("MongoDB Server has been connected")

})

//online connection
//check for whitelist IP
mongoose.connect('mongodb+srv://teejae:1234teejae@b49ecommercetj-ggnuu.mongodb.net/<dbname>?retryWrites=true&w=majority', {

	useNewUrlParser: true,
	useUnifiedTopology: true


})

new ApolloServer({

	typeDefs: require('./graphql/schema'),
	resolvers: require('./graphql/resolvers')

}).applyMiddleware({app, path: '/graphql'})


//instead of having body parser use this
app.use(express.json())
app.use(express.urlencoded({extended: true}))

//on the current directory
const userRoutes = require('./routes/user')

app.use('/api/users', userRoutes)

app.listen(process.env.PORT || 4000, ()=>{

	console.log(`App is running on port ${process.env.port || 4000}`)


})