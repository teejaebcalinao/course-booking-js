const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({


	firstname: {

		type: String,
		required: [true, 'First Name is Required.']

	},
	lastname: {

		type: String,
		required: [true, 'Last Name is Required']

	},
	email: {

		type: String,
		required: [true, 'Email is required']

	},

	loginType: {

		type: String,
		required: [true, 'Login type is required']

	},
	mobileNumber:{

		type: String

	},
	isAdmin: {

		type: Boolean,
		default: false

	},
	enrollments: [

		{

			courseId: {

				type: String,
				required: [true, 'Course ID is required']

			},
			enrolledoN: {

				type: Date,
				default: new Date()

			},
			status: {

				type: String,
				default: 'Enrolled' //Alt values are canncelled and completed

			}

		}


	]




})